program PatherQIP2005;

uses
  Windows, Messages;

{$R window.res}

//{$EXTERNALSYM InitCommonControls}
procedure InitCommonControls; external comctl32 name 'InitCommonControls';

//==============================================================================
//      Interface\Windows define
//==============================================================================
const
  ID_OK = 1;
  ID_EXIT = 2;
  ID_KEY  = 1000;
  ID_MEMO  = 1001;
  err_00 = 'Error:> Sorry, can''t open or not found: qip.exe';
  err_01 = 'Error:> can''t mapping: qip.exe';
  err_02 = 'Error:> crc32 "qip.exe" invalid';
  err_03 = 'Error:> Sorry, an unknown build QIP';
  inf_00 = 'Info:> Check crc32 qip.exe';
  inf_01 = 'Info:> QIP 8095 crc32 Ok';
  inf_02 = 'Info:> QIP 8097 crc32 Ok';
  inf_03 = 'Info:> close: .\qip.exe';
  inf_04 = 'Info:> Happy End =)';
  notice = 'Notice:> Support QIP build 8095,8097 only!';
  coded  = '------------------------'#13#10 +
           'Coded by alexey-m / 2011'#13#10 +
           'Home page: http://alexey-m.ru/';
//==============================================================================
//      QIP buil info define
//==============================================================================
// QIP 2005a build 8095
// CRC32 original:                $A088E7AD
// Offset key crypt\decrypt pwd:  $1B0E2E\$1B0EEC ; $�31A0000
// Offset protect mod. qip.exe:   $229826         ; 74 -> EB (jz -> jmp)
// -----------------------
// QIP 2005a build 8097
// CRC32 original:                $81F8D1F4
// Offset key crypt\decrypt pwd:  $1600F6\$1601B4 ; $�31A0000
// Offset protect mod. qip.exe:   $266A30         ; 74 -> EB (jz -> jmp)
const
  valid_CRC32_8095 = $A088E7AD;
  OffsetKey1_8095:    DWORD = $1B0E2E;
  OffsetKey2_8095:    DWORD = $1B0EEC;
  OffsetProtect_8095: DWORD = $229826;

  valid_CRC32_8097 = $81F8D1F4;
  OffsetKey1_8097:    DWORD = $1600F6;
  OffsetKey2_8097:    DWORD = $1601B4;
  OffsetProtect_8097: DWORD = $266A30;

procedure OllyBugCall;
asm
  DB $EB,$13,$E2,$07,$C9,$53,$C2,$00,$00,$82,$6D,$F1,$77,$FE,$C5,$6B,$E3,$70,$F6,$00,$00
end;

//==============================================================================
//      Tools function
//==============================================================================

function GetCRC32(szBlock: DWORD; Block: Pointer): DWORD; assembler;
asm
(*
  ; IN
  ; ESI = block offset
  ; EDI = block size
  ; OUT
  ; EAX = CRC32
*)
  push esi
  push edi
  push ecx
  push ebx
  mov edi,eax		// szBlock
  mov esi,edx		// Block
  cld
  xor ecx,ecx
  dec ecx
  mov edx,ecx
@@NextByteCRC:
  xor eax,eax
  xor ebx,ebx
  lodsb
  xor al,cl
  mov cl,ch
  mov ch,dl
  mov dl,dh
  mov dh,8
@@NextBitCRC:
  shr bx,1
  rcr ax,1
  jnc @@NoCRC
  xor ax,08320h
  xor bx,0EDB8h
@@NoCRC:
  dec dh
  jnz @@NextBitCRC
  xor ecx,eax
  xor edx,ebx
  dec edi
jnz @@NextByteCRC
  not edx
  not ecx
  mov eax,edx
  rol eax,16
  mov ax,cx
  pop ebx
  pop ecx
  pop edi
  pop esi
end;

function OffsetMem(lpBase: Pointer; Offset: Cardinal): Pointer; assembler;
asm
  add eax,edx
end;

procedure AppendText(hWndDlg: DWORD; txt: String);
begin
  txt:= txt + #13#10#0;
  SendDlgItemMessage(hWndDlg, ID_MEMO, EM_SETSEL, -1, 0);
  SendDlgItemMessage(hWndDlg, ID_MEMO, EM_REPLACESEL, 0, integer(@txt[1]));
end;

//==============================================================================
//      Path qip.exe function
//==============================================================================

procedure PathQIP(hWndDlg: DWORD; Key: DWORD; QIP: PChar);
var
  hFileMap, hFile: THandle;
  pData: Pointer;
  szFile: DWORD;
begin
  SendDlgItemMessage(hWndDlg, ID_MEMO, WM_SETTEXT, 0, 0);
  hFile:= CreateFileA(QIP,
                      GENERIC_WRITE or GENERIC_READ,
                      FILE_SHARE_READ or FILE_SHARE_WRITE,
                      nil,
                      OPEN_EXISTING,
                      FILE_ATTRIBUTE_NORMAL,
                      0);

  if hFile = INVALID_HANDLE_VALUE then AppendText(hWndDlg, err_00) else begin
    szFile:= GetFileSize(hFile, nil);
    hFileMap:= CreateFileMappingA(hFile, nil, PAGE_READWRITE , 0, szFile, nil);

    if hFileMap = INVALID_HANDLE_VALUE then AppendText(hWndDlg, err_01) else begin

      CloseHandle(hFile); CopyFile(QIP, PChar(QIP+'.bak'),False);

      pData:= MapViewOfFile(hFileMap, FILE_MAP_ALL_ACCESS, 0, 0, szFile);
      if pData <> nil then try
        // Check CRC32 qip.exe
        AppendText(hWndDlg, inf_00);
        case GetCRC32(szFile, pData) of
          valid_CRC32_8095:
            begin
              AppendText(hWndDlg, inf_01);
              DWORD(OffsetMem(pData, OffsetKey1_8095)^):= Key;
              DWORD(OffsetMem(pData, OffsetKey2_8095)^):= Key;
              Byte(OffsetMem(pData, OffsetProtect_8095)^):= $EB;
            end;
          valid_CRC32_8097:
            begin
              AppendText(hWndDlg, inf_02);
              DWORD(OffsetMem(pData, OffsetKey1_8097)^):= Key;
              DWORD(OffsetMem(pData, OffsetKey2_8097)^):= Key;
              Byte(OffsetMem(pData, OffsetProtect_8097)^):= $EB;
            end;
          else
            begin
              AppendText(hWndDlg, err_03);
              szFile:= 0;
            end;
        end;
      finally
        UnmapViewOfFile(pData);
      end;
      CloseHandle(hFileMap);
      AppendText(hWndDlg, inf_03);
    end;
    if szFile<>0 then AppendText(hWndDlg, inf_04);
//    AppendText(hWndDlg, coded);
  end;
end;

//==============================================================================
//      Interface\Windows function
//==============================================================================
function WindowProc(hWndDlg, Msg, wParam, lParam: DWORD): LongInt; stdcall;
var
  Key: DWORD;
begin
  Result:= 0;
  case Msg of
    WM_COMMAND:
      case LoWord(wParam) of
        ID_OK:
          begin
            Key:= GetDlgItemInt(hwndDlg, ID_KEY, Bool(nil^), False);
            if Key = 0 then Key:= $1AC3;
            PathQIP(hWndDlg, Key, '.\qip.exe');
          end;
        ID_EXIT:
          begin
            EndDialog(hwndDlg, 0);
            Halt;
          end;
       end;

    WM_INITDIALOG:
      begin
        AppendText(hWndDlg, notice);
        AppendText(hWndDlg, coded);
        SendMessage(GetDlgItem(hwndDlg, ID_KEY), EM_LIMITTEXT, 8, 0);
        Key:= Random($FFFF) + $7FF;
        SetDlgItemInt(hwndDlg, ID_KEY, Key, False);
      end;
    WM_DESTROY:
      begin
        PostQuitMessage(0);
        Result:= 0;
      end;
//    else Result:= DefWindowProc(hwndDlg, Msg, wParam, lParam);
  end;
end;

begin
  InitCommonControls;
  Randomize;
  OllyBugCall;
  DialogBoxParamA(GetModuleHandle(nil), PChar(100), 0, @WindowProc, 0);
end.
